const grunt = require('grunt');
require("load-grunt-tasks")(grunt); // npm install --save-dev load-grunt-tasks

module.exports = function(grunt) {
    grunt.initConfig({
        apidoc: {
            DatabookNode: {
                src: "src/doc/",
                dest: "doc/",
                options: {
                    debug: true
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-apidoc');
    grunt.registerTask('default', ['apidoc']);
}