#!/bin/bash
if [ ! -n "$1" ] ;then
    module=scaner
else
    module=$1
fi
if [ ! -n "$2" ] ;then
    network=test
else
    network=$2
fi
echo $network
echo "starting module - $module ..."
# stop all node app
echo "pm2 stop pm2-$module.json"
pm2 stop pm2-$module.json
echo "rm -rf log/$module"
rm -rf log/$module
echo "mkdir log/$module"
mkdir log/$module

#如果文件夹不存在，创建文件夹
if [ ! -d "./data" ]; then
  echo "data not exist, then mkdir data"
  mkdir data
fi

ps -fe|grep mongod |grep -v grep
if [ $? -ne 0 ]
then
echo "start mongod....."
mongod -f config/mongod.conf
else
echo "mongo is runing....."
fi

# start all node apps in development mode
echo "pm2 start pm2-$module.json --env $network"
pm2 start pm2-$module.json --env $network