'use strict';

import fs from 'fs';
import path from 'path';
import ab2str from 'arraybuffer-to-string';
import request from 'request';
import { CronJob } from "cron";
import { MongoService } from '../core/service/MongoService';
import { eosUtil } from '../core/chain/eos-util';
import Config from "../../config/config";
import mongoose from "mongoose";

const Debug = require('debug')('DataGrid:scaner:daemon');

Debug(__dirname);

const mdb_url = "mongodb://" + Config.mongo.host + ":" + Config.mongo.port + "/" + Config.mongo.dbname;
Debug("mongodb url: %s", mdb_url);
mongoose.connect(mdb_url, {useNewUrlParser: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    // we're connected!
    Debug("Connected to mongodb server!");
});

const filterStorageAction = (actions) => {
    for(let i = 0; i < actions.length; i++) {
        if(actions[i].account === 'aaa111' && ( actions[i].name === 'add' || actions[i].name === 'modify')) {
            return actions[i];
        }
    }
    return null;
};
const getFileContentByHashId = (hashId) => {
    return new Promise((resolve, reject) => {
        request.get({
            url: 'http://' + Config.storjNode.host + ':' + Config.storjNode.port + '/fs/cat/' + hashId,
            json: true // tell http return json instead of string
        }, function optionalCallback(err, httpResponse, body) {
            if (err) {
                reject(err);
            }
            resolve(body);
        });
    });
};

let isScanning = false;
const scanBlocks = () => {
    let lastBlockNum = fs.readFileSync(path.resolve(__dirname, '../../last-block-num.txt'), {encoding:'utf-8'});
    console.log(lastBlockNum);

    eosUtil.eos.getInfo({}).then(ret => {
        let headBlockNum = ret.head_block_num;
        console.log("head block num: ", headBlockNum);
        let i = setInterval(function(){
            Debug("fetch block ", lastBlockNum);
            eosUtil.eos.getBlock(lastBlockNum, (error, block) => {
                if(error) {
                    Debug(error.toString());
                    return;
                }
                isScanning = true;
                for(let j=0; j < block.transactions.length; j++) {
                    const transaction = block.transactions[j];
                    const actions = transaction.trx.transaction.actions;
                    //Debug(actions);
                    let storjAction = filterStorageAction(actions);
                    if(storjAction) {
                        //Debug(block);
                        Debug(storjAction);
                        getFileContentByHashId(storjAction.data.hash_meta).then(content => {
                            //Debug(content.data);
                            const uintArray = new Uint8Array(content.data);
                            const strExtra = ab2str(uintArray);
                            //Debug(extra);
                            if(storjAction.name === "add") {
                                Debug("add new asset");
                                const metadataTHX = {
                                    block: lastBlockNum,
                                    thxId: transaction.trx.id,
                                    account: storjAction.data.name,
                                    hashId: storjAction.data.hash_data
                                };
                                metadataTHX.extra = JSON.parse(strExtra);
                                metadataTHX.timestamp = block.timestamp;
                                metadataTHX.size = Buffer.byteLength(strExtra);
                                Debug(metadataTHX);
                                return MongoService.deleteThenAddMetadataTHX(metadataTHX);
                            } else if(storjAction.name === "modify") {
                                Debug("update asset metadata");
                                return MongoService.updateMetadata(
                                    { hashId: storjAction.data.hash_data },
                                    { extra: JSON.parse(strExtra) }
                                )
                            }
                        }).then(metadataTHXObj => {
                            Debug(metadataTHXObj);
                        }).catch(error => {
                            Debug(error.toString());
                        });
                    }
                }
            });
            lastBlockNum++;
            if(lastBlockNum > headBlockNum) {
                isScanning = false;
                fs.writeFileSync(path.resolve(__dirname, '../../last-block-num.txt'), headBlockNum, {encoding:'utf-8'});
                Debug("update last block number to %s", headBlockNum);
                clearInterval(i);
            }
        }, 1000);
    }).catch(error => {
        console.log(error.toString());
    });
};



new CronJob('*/10 * * * * *', function () {
    Debug("scanning: ", isScanning);
    if(!isScanning) {
        Debug("last scan was done, kickoff new scanning...");
        scanBlocks();
    } else {
        Debug("last scanning is in progress, skip");
    }

}, null, true, 'Asia/Chongqing');


