'use strict';

require('dotenv').config();

import chai from 'chai';
import request from 'request';

const expect = chai.expect;
const should = chai.should();


console.log("env.type: ", process.env.TYPE);
let sosnode_port = 3003;   // default to test port
if(process.env.TYPE === '1') { // product environment
    sosnode_port = 9627;
}
console.log("port: ", sosnode_port);

let host = '47.99.61.209';
if(process.env.NETWORK === 'local') { // local environment
    host ="127.0.0.1";
}
console.log("host: ", host);

describe('.apiRouter (files)', function () {
    this.timeout(120 * 1000);
    before((done) => {
        done();
    });
    after((done) => {
        done();
    });
    it('api.find', function (done) {
        const queryBody = {
            "size": { $gte: 10},
            "timestamp": { $gte: new Date(2018, 7, 10) },
            "stringMatch": {
                "extra.desc": "space"
            }
        };
        request.post({
            url: 'http://' + host +':'  + sosnode_port + '/find?page=1&pageSize=2&sortBy=timestamp&order=1',
            json: true, // tell http return json instead of string
            body: queryBody }, function optionalCallback(err, httpResponse, body) {
            if (err) {
                return done(err);
            }
            console.log("server response: ", JSON.stringify(body));
            body.should.have.property('docs');
            body.should.have.property('total');
            body.should.have.property('page');
            body.total.should.not.equal(0);
            body.page.should.equal(1);
            body.limit.should.equal(2);
            done();
        });
    });
});
