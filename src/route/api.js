'use strict';

import url from 'url';
import { Router } from 'restify-router';
import { MongoService } from '../core/service/MongoService';

const Debug = require('debug')('DataGrid:scaner:route:api');

export const router = new Router();

const sortFields = ['_id', 'size', 'timestamp'];
const sortOrders = [-1, 1];

router.post('/find', function (req, res, next) {
    Debug("find");
    Debug("request.headers: %s", JSON.stringify(req.headers));
    const bodyType = typeof req.body;
    Debug("bodyType: ", bodyType);

    if (bodyType === 'string') {
        req.body = JSON.parse(req.body);
    }
    Debug(JSON.stringify(req.body));
    let queryBody = {};
    // organize valid query object by filter out string match queries
    Object.keys(req.body).forEach(function(key) {
        if(key !== 'stringMatch') {
            queryBody[key] = req.body[key];
        } else {
            Object.keys(req.body.stringMatch).forEach(function(strkey) {
                queryBody[strkey] = new RegExp(req.body.stringMatch[strkey], 'i');
            });
        }
    });
    Debug(queryBody);

    let page = 1;
    let pageSize = 15;
    let sortField = "_id";
    let order = -1;

    const args = url.parse(req.url, true).query;
    Debug(args);

    if (args) {
        if (args.sortBy) {
            sortField = args.sortBy;
            const indexOfsortField = sortFields.indexOf(sortField);
            if(indexOfsortField === -1) { // not standard query fields: ['_id', 'size', 'timestamp']
                if(!sortField.startsWith('extra.')) { // not standard fields, not extra fields ether
                    res.send(400, 'wrong query fields');
                    return next();
                }
            }
        }
        if (args.order) {
            if(!isNaN(Number(args.order))) {
                const indexOfOrder = sortOrders.indexOf(Number(args.order));
                if(sortOrders.indexOf(order) !== -1) {
                    order = sortOrders[indexOfOrder];
                }
            }
        }
        if(args.page) {
            if(!isNaN(Number(args.page))) {
                page = Number(args.page);
            }
        }
        if(args.pageSize) {
            if(!isNaN(Number(args.pageSize))) {
                pageSize = Number(args.pageSize);
                pageSize = pageSize >= 50 ? 50 : pageSize;
            }
        }
    }
    MongoService.find(queryBody, page, pageSize, sortField, order).then(result => {
        res.send(200, result);
        return next();
    }).catch(error => {
        res.send(500, error.message);
        return next();
    });
});
